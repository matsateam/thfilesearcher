package net.ukr.wumf;

import java.io.File;

public class SearchTask implements Runnable {

	private ThFileSearcher mfs;
	private Thread thr;
	private File startDir;
	private String soughtFileName;

	public SearchTask(ThFileSearcher mfs, File startDir, String soughtFileName) {
		super();
		this.mfs = mfs;
		this.startDir = startDir;
		this.soughtFileName = soughtFileName;
		thr = new Thread(this);
		thr.start();
	}

	public Thread getThr() {
		return thr;
	}

	public void setThr(Thread thr) {
		this.thr = thr;
	}

	@Override
	public void run() {
		System.out.println(thr.getName() + " start.");
		mfs.fileSearcher(startDir, soughtFileName);
	}

}
